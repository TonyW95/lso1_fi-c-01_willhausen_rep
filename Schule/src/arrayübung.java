import java.util.Random;
import java.util.Scanner;


public class arrayübung {

	static Scanner tast = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int anzahl;
		int[] zahlenArray;
		double mittelwert;
		
		anzahl = benutzereingabe("Wie viele Zahlen soll das Array haben:");
		zahlenArray = zufallszahlen(anzahl);
		ausgabeArray(zahlenArray);
		mittelwert = mittelwertBerechnung(zahlenArray,anzahl);
		ausgabe(mittelwert);
		

	}
	
	public static int benutzereingabe(String text) {
		System.out.print(text);
		int y = tast.nextInt();
		return y;
	}
	public static int[] zufallszahlen(int anzahl) {
		Random zufall = new Random();
		int[] zufallarray = new int[anzahl];
		
		for(int i= 0; i < anzahl; i++) {
			zufallarray[i] = zufall.nextInt(99);
		}
		return zufallarray;
	}
	
	public static double mittelwertBerechnung (int[] meinArray, int anzahl) {
		int summe = 0;
		double mittel = 0;
		
		for (int i = 0; i < meinArray.length; i++) {
			summe = summe + meinArray[i]; //summe += meinArray[i]; 
		}
		
		mittel = summe / anzahl;
		return mittel;
		
	}

	public static void ausgabeArray(int[] meinArray) {
		
		for (int i = 0; i < meinArray.length; i++) {
			System.out.printf("%3d",meinArray[i]);
	}
	}
	public static void ausgabe(double mittelwert) {
		System.out.printf("\nDer Mittelwert ist: %.2f", mittelwert);
	}
}
