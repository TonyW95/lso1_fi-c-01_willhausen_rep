import java.util.Scanner;

public class Mittelwert {
	
	
	//Methode kann nur auf diesen Block zugreifen und gibt Double wie nach static beschrieben aus 
	// und kann mit verarbeitung ausgegeben
	
	//für die Lesbarkeit, um den Block (Methode(verarbeitung)) mehrmals wiederzuverwenden und abzukürzen
	public static double verarbeitung(double pX, double pY) {
		
		double ergebnis;
		ergebnis = ( pX + pY ) / 2;
		return ergebnis;
	}

	// static= statsich kein eigen erstelltes objekt
   public static void main(String[] args) {
	    
	   // Sacanner der Tastaur(input)
	   Scanner tastatur = new Scanner(System.in);

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x;
      double y;
      double m;
      
      //Festlegen des Scanners(Objects) mit einer Variable, für die ausgabe des Tastaur befehls
      System.out.print("erster wert: ");
      x = tastatur.nextDouble();
      
      System.out.print("zweiter wert: ");
      y = tastatur.nextDouble();
      
      
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
    //  m = (x + y) / 2.0;
      
      m = verarbeitung(x, y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
}