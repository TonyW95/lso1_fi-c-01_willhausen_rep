
import java.util.Scanner;



public class PCHaendler {
	
	static Scanner myScanner = new Scanner(System.in);
	
	public static String liesString(String text) {
		
		System.out.print(text);
		String artikel = myScanner.next();
		
		return artikel;
	}
	public static int liesInt(String text) {
		
		System.out.print(text);
		int anzahl = myScanner.nextInt();
		
		return anzahl;
	}
	public static double liesDouble(String text) {
		
		System.out.print(text);
		double preis = myScanner.nextDouble();
		
		return preis;
	}
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		
		 
		
		return anzahl * nettopreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		
		
		return nettogesamtpreis * (1 + mwst / 100); 
	}
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		

		System.out.println("\t Rechnung:");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f€ %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f€ (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	
	public static void main(String[] args) {
		

		// Benutzereingaben lesen
		String artikel = liesString("was moechten Sie bestellen?");

		int anzahl = liesInt("Geben Sie die Anzahl ein:");

		double preis = liesDouble("Geben Sie den Nettopreis ein:");

		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		

		// Verarbeiten
		
		
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
				

		// Ausgeben
		
		rechnungausgeben(artikel,anzahl,nettogesamtpreis,bruttogesamtpreis,mwst);
	}
}
//  
// 
