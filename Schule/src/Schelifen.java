import java.util.Scanner;

public class Schelifen {

	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		
		
		System.out.println("(1).Quadrat größe");
		System.out.println("(2).Matrix");
		
		System.out.print("Welche Schleifen Aufgaben möchten Sie benutzen, geben sie bitte eine Zahl von (1 bis 2) ein: ");

		byte aufgabe = tastatur.nextByte();

		if (aufgabe == 1)
			aufgabefor8();
		else 
			aufgabewhile8();
		
	}
		public static void aufgabefor8() {
	
		System.out.println("Länge des Quadrates:");
		int size = tastatur.nextInt();
		
	    for (int x = 0; x <= size; x++) {
	        for (int y = 0; y <= size; y++) {
	            if (x == 0 || x == size || y == 0 || y == size  ) {
	                System.out.printf("%2s","*");
	            } else {
	                System.out.print("  ");
	            }
	        }
	        System.out.print("\n");
	    }
	}
		
		public static void aufgabewhile8() {
			
			System.out.print("eingabe der Zahl");
			int zahl = tastatur.nextInt();
			int i = 0;
			
			while(i<100) {
				if (!(i%zahl == 0 || i == quersumme(zahl)) || i==0)
					System.out.printf("%5d",i);
				else
					System.out.printf("%5s" , "*");
				i++;
				if (i%10 == 0 && i!=0)
					System.out.println();
				
			}
		}
			
			public static int quersumme(int zahl) {

				if (zahl <= 9) return zahl;

				return zahl%10 + quersumme(zahl/10);
		}
	}

