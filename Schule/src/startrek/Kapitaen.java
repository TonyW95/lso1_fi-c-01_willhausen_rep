package startrek;

public class Kapitaen {

	//Attribute
	private String kapitaen;
	private int steuertseid;

	//Konstruktor
	public Kapitaen() {
		
	}
	
	public Kapitaen(String kapitaen, int steuertseid) {
		this.kapitaen = kapitaen;
		this.steuertseid = steuertseid;
	}
	
	//getter und setter
	public String getKapitaen() {
		return kapitaen;
	}
	public void setKapitaen(String kapitaen) {
		this.kapitaen = kapitaen;
	}
	public int getSteuerseid() {
		return steuertseid;
	}
	public void setSteuertseid(int steuertseid) {
		this.steuertseid = steuertseid;
	}
	
}
