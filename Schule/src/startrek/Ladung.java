package startrek;

	/**
	 * Dies ist die Klasse Ladung. Ladung dient zum Verwalten der einzelnen Ladungen der Raumschiffe
	 * mit Anzahl und Name der Ladung.
	 * 
	 * @author tonywillhausen
	 * @version 1.1
	 * @since JDK 14.0.2
	 */

public class Ladung {
	
	private String name;
	private int anzahl;

	/**
	 * Standard Konstruktor für die Klasse Ladung.
	 */
	
	public Ladung() {

	}

	/**
	 * Vollparametrisierter Konstruktor für die Klasse Ladung.
	 * @param name der Name der Ladung.
	 * @param anzahl die Anzahl der Ladung.
	 */
	
	public Ladung(String name, int anzahl) {
		this.name = name;
		this.anzahl = anzahl;
	}

	/**
	 * Liefert die Anzahl der Ladung.
	 * @return gibt die Anzahl vom Datentyp int der Ladung zurück.
	 */
	
	public int getAnzahl() {
		return anzahl;
	}
	
	/**
	 * Setzt die Anzahl der Ladung.
	 * @param anzahl setzt die Anzahl der Ladung als Datentype int.
	 */

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	/**
	 * Liefert den Namen der Ladung.
	 * @return gibt den Namen vom Datentyp String der Ladung zurück.
	 */
	
	public String getName() {
		return name;
	}

	/**
	 * Setzt den Namen der Ladung.
	 * @param name setzt den Namen der Ladung als Datentyp String.
	 */
	
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Diese Methode Überschreibt den toString der Methode ladungsverzeichnis um das Objekt Ladung als String in der Konsole auszugeben.
	 */
	
	@Override
	public String toString() {
		return getAnzahl() + "x " + getName();
	}
}
