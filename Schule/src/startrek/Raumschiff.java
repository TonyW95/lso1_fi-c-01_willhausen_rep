package startrek;

import java.util.ArrayList;
import java.util.Random;

/**
 * Dies ist die Klasse Raumschiff. Raumschiff dient zum Verwalten der Raumschiffe
 * mit Namen, Statuswerte , Ladungsliste, BroadcastKommunikator eines Raumschiffes.
 * 
 * @author Tony Willhausen
 * @version 1.5
 * @since JDK 14.0.2
 *
 */

public class Raumschiff {

	// Attribute
	private String name;
	private int energie;
	private int schutzschilde;
	private int lebenserhaltung;
	private int huelle;
	private int photontorpedo;
	private int reperaturandroiden;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsliste = new ArrayList<Ladung>();

	/**
	 * Standard Konstruktor fuer die Klasse Raumschiff.
	 */
	public Raumschiff() {
	}

	/**
	 * Vollparametrisierter Konstruktor fuer die Klasse Raumschiff.
	 * @param name der Name der Raumschiffe.
	 * @param energie die Energie in Prozent der Raumschiffe.
	 * @param schutzschilde die Schutzschilde in Prozent der Raumschiffe.
	 * @param lebenserhaltung die Lebenserhaltung in Prozent der Raumschiffe.
	 * @param huelle die Hülle in Prozent der Raumschiffe.
	 * @param photontorpedo die Anzahl an Photontorpedos in den Raumschiffen.
	 * @param reperaturandroiden die Anzahl an Reperaturandroiden in den Raumschiffen.
	 */
	public Raumschiff(String name, int energie, int schutzschilde, int lebenserhaltung, int huelle, int photontorpedo,
			int reperaturandroiden) {
		this.name = name;
		this.energie = energie;
		this.schutzschilde = schutzschilde;
		this.lebenserhaltung = lebenserhaltung;
		this.huelle = huelle;
		this.photontorpedo = photontorpedo;
		this.reperaturandroiden = reperaturandroiden;
	}

	/**
	 * Liefert den Namen des Raumschiffs.
	 * @return gibt den Namen vom Datentyp Sting des Raumschiffs zurück.
	 */
	// Getter und Setter
	public String getName() {
		return name;
	}

	/**
	 * Setzt den Namen des Raumschiffs.
	 * @param name setzt den Namen des Raumschiffs als Datentype String.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Liefert die Energie des Raumschiffs in Prozent.
	 * @return gibt die Energie vom Datentyp Int des Raumschiffs zurück.
	 */
	public int getEnergie() {
		return energie;
	}

	/**
	 * Setzt die Energie des Raumschiffs in Prozent.
	 * @param energie setzt die Energie des Raumschiffs als Datentype Int.
	 */
	public void setEnergie(int energie) {
		this.energie = energie;
	}

	/**
	 * Liefert die Schutzschilde des Raumschiffs in Prozent.
	 * @return gibt die Schutzschilde vom Datentyp Int des Raumschiffs zurück.
	 */
	public int getSchutzschilde() {
		return schutzschilde;
	}

	/**
	 * Setzt die Schutzschilde des Raumschiffs in Prozent.
	 * @param schutzschilde setzt die Schutzschilde des Raumschiffs als Datentype Int.
	 */
	public void setSchutzschilde(int schutzschilde) {
		this.schutzschilde = schutzschilde;
	}

	/**
	 * Liefert die Lebenserhaltng des Raumschiffs in Prozent.
	 * @return gibt die Lebenserhaltung vom Datentyp Int des Raumschiffs zurück.
	 */
	public int getLebenserhaltung() {
		return lebenserhaltung;
	}

	/**
	 * Setzt die Lebenserhaltung des Raumschiffs in Prozent.
	 * @param lebenserhaltung setzt die Lebenserhaltung des Raumschiffs als Datentype Int.
	 */
	public void setLebenserhaltung(int lebenserhaltung) {
		this.lebenserhaltung = lebenserhaltung;
	}

	/**
	 * Liefert die Huelle des Raumschiffs in Prozent.
	 * @return gibt die Huelle vom Datentyp Int des Raumschiffs zurück.
	 */
	public int getHuelle() {
		return huelle;
	}

	/**
	 * Setzt die Huelle des Raumschiffs in Prozent.
	 * @param huelle setzt die Huelle des Raumschiffs als Datentype Int.
	 */
	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}

	/**
	 * Liefert die Anzahl der Photontorpedos des Raumschiffs.
	 * @return gibt die Potonthorpedos vom Datentyp Int des Raumschiffs zurück.
	 */
	public int getPhotontorpedo() {
		return photontorpedo;
	}

	/**
	 * Setzt die Anzahl der Photontorpedos des Raumschiffs.
	 * @param photontorpedo setzt die Photontorpedos des Raumschiffs als Datentype Int.
	 */
	public void setPhotontorpedo(int photontorpedo) {
		this.photontorpedo = photontorpedo;
	}

	/**
	 * Liefert die Anzahl der Reperaturandroiden des Raumschiffs.
	 * @return gibt die Reperaturandroiden vom Datentyp Int des Raumschiffs zurück.
	 */
	public int getReperaturandroiden() {
		return reperaturandroiden;
	}

	/**
	 * Setzt die Anzahl der Reperaturandroiden des Raumschiffs.
	 * @param reperaturandroiden setzt die Reperaturandroiden des Raumschiffs als Datentype Int.
	 */
	public void setReperaturandroiden(int reperaturandroiden) {
		this.reperaturandroiden = reperaturandroiden;
	}

	// Methoden

	/**
	 * Diese Metohde vergleicht ob Photontorpedos größer als  0 ist.
	 * wenn True wird die Methode kommunikator aufgerufen mit der Nachricht an alle "Photonentorpedos abgeschossen" ,
	 * reduziert den Photontorpedo um eins von einem beliebigen angreifenden Raumschiff 
	 * und ruft die Methode treffer auf mit dem Parameter raumschiff.
	 * 
	 * Sonst wird Methode kommunikator aufgerufen mit der Nachricht an alle -=*Click*=-.
	 *
	 * @param raumschiff gibt an welches Raumschiff beschschossen wird als Objekt.
	 */
	public void photonentorpedos(Raumschiff raumschiff) {
		if (getPhotontorpedo() > 0) {
			kommunikator("Photonentorpedos abgeschossen");
			photontorpedo--;
			treffer(raumschiff);
		} else {
			kommunikator("-=*Click*=-");
		}
	}
	
	/**
	 * Diese Methode vergleicht ob Energie größer gleich 50 ist.
	 * 
	 * Wenn True wird die Methode kommunikator aufgerufen mit der Nachricht an alle "Phaserkanone abgeschossen",
	 * die Energie des angreifenden Raumschiffs wird um 50 reduziert
	 * und ruft die Methode treffer auf mit dem Parameter raumschiff.
	 * 
	 * Sonst wird Methode kommunikator aufgerufen mit der Nachricht an alle -=*Click*=-.
	 * 
	 * @param raumschiff gibt an welches Raumschiff beschossen wird als Objekt.
	 */

	// Phaserkanone abschießen
	public void phaserkanone(Raumschiff raumschiff) {
		if (getEnergie() >= 50) {
			kommunikator("Phaserkanone abgeschossen");
			setEnergie(getEnergie() - 50);
			treffer(raumschiff);
		} else {
			kommunikator("-=*Click*=-");
		}
	}
	
	/**
	 * Diese Methode gibt als Nachricht den Namen des Raumschiffes in der Konsole aus welches getroffen wurde.
	 * 
	 * Vergleicht ob vom getroffenen Raumschiff die Schutzschilde größer als 0 sind und reduziert die Schilde um 50.
	 * 
	 * Sonst wird die Hülle und Energie des getroffenen Raumschiffes um jewails 50 reduziert,
	 * wenn danach die Hülle kleiner gleich 0 ist wird die Methode kommunikator aufgerufen und die Nachricht an alle  ausgegeben 
	 * dass die Lebenserhaltungssysteme vernichtet worden sind.
	 * 
	 * 
	 * @param raumschiff gibt an welches Raumschiff getroffen wurde als Objekt.
	 */

	// Treffer vermerken
	private void treffer(Raumschiff raumschiff) {
		System.out.println(raumschiff.getName() + " wurde getroffen\n");
		if (raumschiff.getSchutzschilde() > 0) {
			raumschiff.setSchutzschilde(raumschiff.getSchutzschilde() - 50);
		} else {
			raumschiff.setHuelle(raumschiff.getHuelle() - 50);
			raumschiff.setEnergie(raumschiff.getEnergie() -50);
			if (raumschiff.getHuelle() <= 0) {
				kommunikator("\nLebenserhaltungssysteme der " + raumschiff.getName() + " wurden vernichtet");
			}
		}
	}

	/**
	 * Die Methode logbuch gibt broadcastKommunikator zurück.
	 * 
	 * @return gibt den Inhalt des broadcastKommunikator als ArrayList zurück.
	 */
	
	// Logbuch Einträge zurückgeben
	public ArrayList<String> logbuch() {
		return broadcastKommunikator;
	}

	/**
	 * Diese Methode fügt eine beliebige Ladung zur Ladungsliste eines Raumschiffs hinzu.
	 * 
	 * @param neueLadung gibt an welche Ladung beladen wird als Objekt.
	 */
	
	// beladen der Raumschiffe mit Ladung
	public void ladungbeladen(Ladung neueLadung) {
		ladungsliste.add(neueLadung);
	}

	/**
	 * Die Methode beladenTorpedos, gibt als Nachricht "Keine Photonentorpedos gefunden!" in der Kosnole aus
	 * und ruft die Methode kommunikator auf mit dem String "-=*Click*=-", wenn keine Photontorpedos in der Ladung sind.
	 * 
	 * Wenn die anzahlTorpedos größer ist als die vorhandene anzahl Photontorpedos im Raumschiff,
	 * wird  Ladungsmenge Photonentorpedos über die Setter Methode vermindert und die Anzahl der Photonentorpedo im Raumschiff erhöht.
	 * Die eingesetzte Anzahl an Photonentorpedos wird als Nachricht in der Konsole ausgegeben.
	 * 
	 * Sonst bleibt die Anzahl an geladenen Photontorpedos auf dem Raumschiff bestehen.
	 * 
	 * @param anzahlTorpedos gibt an wieviele Photontorpedos in der Klasse Ladung sind, als Datentyp int.
	 */
	
	// Ladung “Photonentorpedos” einsetzen
	public void beladenTorpedos(int anzahlTorpedos) {
		if (anzahlTorpedos == 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			kommunikator("-=*Click*=-");
		}
		if (anzahlTorpedos > getPhotontorpedo()) {
			this.setPhotontorpedo(this.getPhotontorpedo() + anzahlTorpedos);
			ladungsliste.stream().filter(Ladung -> Ladung.getName().equals("Photontorpedo"))
						.forEach(Ladung -> Ladung.setAnzahl(Ladung.getAnzahl() - anzahlTorpedos));
			System.out.println(getPhotontorpedo() + " Photonentorpedo(s) eingesetz\n");
		}
	}

	/**
	 * Diese Methode entfernt Objekte aus der ladungsliste vom Raumschoff wenn die Anzahl 0 ist.
	 */
	
	// Ladungsliste aufräumen wo die anzahl 0 ist wird entfernt.
	public void ladungaufräumen() {
		ladungsliste.removeIf(Ladung -> Ladung.getAnzahl() == 0);
	}

	/**
	 * Diese Methode gibt die ladungsliste des Raumschiffes in der Kosnole aus.
	 */
	
	// Ladungsverzeichnis ausgeben
	public void ladungsverzeichnis() {
		System.out.println("Ladungsverzeichnis von " + getName() + ": ");
		for (Ladung ladung : ladungsliste) {
			System.out.println(ladung);
		}
		System.out.println("");
	}
	
	/**
	 * Diese Methode gibt die nachricht des Schiffes an alle in der Konsole aus und fügt die nachricht dem broadcastKommunikator hinzu.
	 * 
	 * @param nachricht die Nachricht an alle Datentyp String wieder.
	 */

	// Nachrichten an Alle
	public void kommunikator(String nachricht) {
		System.out.println("Nachricht an alle von " + getName()+ " : " + nachricht + "\n");
		broadcastKommunikator.add(nachricht);
	}
	
	/**
	 * Die Methode gibt den gesamten Zustand des Raumschiff und den entsprechenden Namen in der Konsole aus.
	 * 
	 */

	// Der Zustand des Raumschiffes ausgegebe
	public void zustand() {
		System.out.println("Raumschiffname: " + getName());
		System.out.println("Energie: " + getEnergie() + "%");
		System.out.println("Schutzschilde: " + getSchutzschilde() + "%");
		System.out.println("Lebenserhaltung: " + getLebenserhaltung() + "%");
		System.out.println("Hülle: " + getHuelle() + "%");
		System.out.println("Photontorpedo: " + getPhotontorpedo());
		System.out.println("ReperaturAndroiden: " + getReperaturandroiden() + "\n");
	}

	/**
	 * Die Methode entscheidet anhand der Übergebenen Parameter welche Schiffsstrukturen repariert werden sollen.
	 * Ist die übergebene Anzahl an Andrioden größer and die Anzahl an Androiden im Raumschiff, dann wird  wird die vorhandene Anzahl von Androiden für die Rechnung eingesetzt.
	 * 
	 * Für die Prozentuale Berechnung der zu repariedenen Schiffstrukturen wird eine Random Zahl zwischen 0-100 generiert * der vorhandenen Anzahl von Androiden / anzahl der auf True gesetzten Schiffstrukturen.
	 * Das Ergebnis wird auf die true gesetzten Schiffstrukturen hinzugefügt.
	 * 
	 * @param huelleb gibt an ob Hülle repariert werden muss als Datentyp boolean.
	 * @param schutzschildeb gibt an ob Schutzschilde repariert werden muss als Datentyp boolean.
	 * @param energieb gibt an ob Energie repariert werden muss als Datentyp boolean.
	 * @param anzahlAndroiden gibt an wieviele Androiden übergeben werden als Datentyp int.
	 */
	
	// Reparieren der Raumschiff Strukturen(Hülle, Schild, Energie, Menge der
	// random zahl * anzahl reperatur / menge true werten
	public void reperatur(boolean huelleb, boolean schutzschildeb, boolean energieb, int anzahlAndroiden) {
		Random randomzahl = new Random();
		int anzahlTrueStrukturen = 0;
		int ergebnis = 0;

		if (huelleb) {
			anzahlTrueStrukturen++;
		}

		if (schutzschildeb) {
			anzahlTrueStrukturen++;
		}

		if (energieb) {
			anzahlTrueStrukturen++;
		}

		if (anzahlAndroiden >= getReperaturandroiden()) {
		ergebnis = randomzahl.nextInt(100) * getReperaturandroiden() / anzahlTrueStrukturen;
		}
		else {
		ergebnis = randomzahl.nextInt(100) * anzahlAndroiden / anzahlTrueStrukturen;
		}
		
		if (schutzschildeb) {
			setSchutzschilde(getSchutzschilde() + ergebnis);

		}
		if (energieb) {
			setEnergie(getEnergie() + ergebnis);
		}
		if (huelleb) {
			setHuelle(getHuelle() + ergebnis);
		}
	}
}
