package startrek;

 /**
  * Dies ist die Klasse Play. Play dient zu Deklaration und Initialisierung der Objekte und aufrufen, ausführen der Methoden.
  * @author tonywillhausen
  * @version 1.1
  * @since JDK 14.0.2
  */

public class Play {

	public static void main(String[] args) {

		
		/**
		 * Deklaration und Initialisierung der Objekte
		 */
		
		Raumschiff klingonen = new Raumschiff("IKS Hegh`ta", 100, 100, 100, 100, 1, 2);

		Ladung schneckensaft = new Ladung("Ferengi", 200);
		Ladung schwert = new Ladung("Bat`leth Klingonen Schwert", 200);
		
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);

		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		Ladung roteMaterie = new Ladung("Rote Materie", 2);
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
		
		Raumschiff vulkanier = new Raumschiff("Ni`Var", 80, 80, 100, 50, 0, 5);

		Ladung forschungssonden = new Ladung("Forschungssonden", 35);
		Ladung photontorpedo = new Ladung("Photontorpedo", 3);

		/**
		 * aufrufen der Methode ladungbeladen um die Ladungen dem jeweiligen Raumschiff hinzuzufügen.
		 */
		
		klingonen.ladungbeladen(schneckensaft);
		klingonen.ladungbeladen(schwert);
		
		romulaner.ladungbeladen(borgSchrott);
		romulaner.ladungbeladen(roteMaterie);
		romulaner.ladungbeladen(plasmaWaffe);
		
		vulkanier.ladungbeladen(photontorpedo);
		vulkanier.ladungbeladen(forschungssonden);

//----------------
		
		/*
		 * Die Klingonen schießen mit dem Photonentorpedo einmal auf die Romulaner.
		 */
		klingonen.photonentorpedos(romulaner);
		/*
		 * Die Romulaner schießen mit der Phaserkanone zurück.
		 */
		romulaner.phaserkanone(klingonen);
		/*
		 * Die Vulkanier senden eine Nachricht an Alle “Gewalt ist nicht logisch”.
		 */
		vulkanier.kommunikator("Gewalt ist nicht logisch");
		/*
		 * Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
		 */
		klingonen.zustand();
		klingonen.ladungsverzeichnis();
		/*
		 * Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein.
		 */
		vulkanier.reperatur(true, true, true, vulkanier.getReperaturandroiden());
		/*
		 * Die Vulkanier verladen Ihre Ladung “Photonentorpedos” in die Torpedoröhren Ihres Raumschiffes und räumen das Ladungsverzeichnis.
		 */
		vulkanier.beladenTorpedos(photontorpedo.getAnzahl());
		vulkanier.ladungaufräumen();
		/*
		 * Die Klingonen schießen mit zwei weiteren Photonentorpedo auf die Romulaner.
		 */
		klingonen.photonentorpedos(romulaner);
		klingonen.photonentorpedos(romulaner);
		/*
		 * Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
		 */
		klingonen.zustand();
		klingonen.ladungsverzeichnis();
		romulaner.zustand();
		romulaner.ladungsverzeichnis();
		vulkanier.zustand();
		vulkanier.ladungsverzeichnis();
		/*
		 * Geben Sie den broadcastKommunikator aus.
		 */
		vulkanier.logbuch();

	}
}
