package workout;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;



public class workout {

	static Scanner tastatur = new Scanner(System.in);

	public static ArrayList<String> reader (String csvFile,String line,String csvSplitby) throws IOException,FileNotFoundException {
		ArrayList<String> myList = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			while ((line = br.readLine()) != null) {

				String[] data = line.split(csvSplitby);
				myList.addAll(Arrays.asList(data[0]));
			}
		}
		return myList;
	}
	public static ArrayList<String> besucht (String csvFile,String line,String csvSplitby) throws IOException,FileNotFoundException{
		ArrayList<String> myDate = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
			System.out.println("Besuchte Orte:\n");
			while ((line = br.readLine()) != null) {

				String[] data = line.split(csvSplitby);

				myDate.addAll(Arrays.asList(data[0]));
				System.out.println("Ortsteil: " + data[0] + " Besucht am: " + data[1]);
			}
		}
//		System.out.print(myDate);
		return myDate;
	}

	
	public static void generator(ArrayList<String> myList,ArrayList<String> myDate,String date,String newworkoutcsv) {

		Random r = new Random();
	    int randomitem = r.nextInt(myList.size());
	    String randomElement = myList.get(randomitem);
	    System.out.println(randomElement);
	    System.out.println(myDate);
	     if (myList.equals(myDate))
	    	 JOptionPane.showMessageDialog(null, "Alle Orte Besucht");
	     else if (!(myDate.contains(randomElement)))
//	    System.out.print(date);
			   try {
				   FileWriter fw = new FileWriter(newworkoutcsv,true);
				   BufferedWriter bw = new BufferedWriter(fw);
				   PrintWriter pw = new PrintWriter(bw);
				   
				   pw.println(randomElement+";"+date);
				   pw.flush();
				   pw.close();
				   
			    JOptionPane.showMessageDialog(null, "FERTIG");
			   
             }
			   catch (Exception e) {
				// TODO: handle exception
				   }
	    	else
	    		JOptionPane.showMessageDialog(null, "FEHLER");
	    	

	}
	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
	
		
		String newworkoutcsv = "/Users/tonywillhausen/Desktop/newworkout.csv";
		String workoutcsv = "/Users/tonywillhausen/Desktop/workout.csv";
		String line = "";
		String csvSplitby = ";";
		
		ArrayList<String> myDate = besucht(newworkoutcsv,line,csvSplitby);
		ArrayList<String> orte = reader(workoutcsv,line,csvSplitby);	
		

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		sdf.setLenient(false);
		
		while(true) {	
		System.out.print("\n\n\nWann möchten Sie den nächsten zufälligen Ort besuchen:");
		String date = tastatur.next();
	      
		try {
	          Date date1 = sdf.parse(date);
	          

	          
	          sdf = new SimpleDateFormat("EEE, d MMM yyyy");
	          String date2 = sdf.format(date1);
	          System.out.println("Datum: " + sdf.format(date1));
	          generator (orte,myDate,date2,newworkoutcsv);
	       } catch (ParseException e) {
	          System.out.println("Dieses Datum gibt es nicht, bitte wiedeholen sie Ihre Eingabe");
	       }
		}
	}

}
