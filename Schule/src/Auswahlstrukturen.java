import java.util.Scanner;

public class Auswahlstrukturen {

	static Scanner tastatur = new Scanner(System.in);

	public static char abfrage() {
		System.out.print("neue Aufgabe? y/n:");
		char wert = tastatur.next().charAt(0);
		if (wert == 'y')
			return wert;
		else if (wert == 'n')
			System.out.println("Goodbye !!!");
		return wert;
	}

	public static void main(String[] args) {

		do {
			byte aufgabe = 0;
			do {
				System.out.println("(1).Note");
				System.out.println("(2).Steuersatz");
				System.out.println("(3).Monate/a");
				System.out.println("(4).Monate/b");
				System.out.println("(5).Hardware-Großhändler");
				System.out.println("(6).Rabattsystem");
				System.out.println("(7).BMI Rechner");
				System.out.println("(8).Römische Zahl in Dezimalzahl");
				System.out.println("(9).Sortieren");
				System.out.println("(10).Schaltjahr");
				System.out.println("(11).Römische Zahlen in Dezimalzahlen");

				System.out.print("Welche Auswahlstruktur Aufgaben möchten Sie benutzen, geben sie bitte eine Zahl von (1 bis 11) ein: ");

				aufgabe = tastatur.nextByte();

				if (aufgabe == 1)
					aufgabe1();
				else if (aufgabe == 2)
					aufgabe2();
				else if (aufgabe == 3)
					aufgabe3a();
				else if (aufgabe == 4)
					aufgabe3b();
				else if (aufgabe == 5)
					aufgabe4();
				else if (aufgabe == 6)
					aufgabe5();
				else if (aufgabe == 7)
					aufgabe6();
				else if (aufgabe == 8)
					aufgabe7();
				else if (aufgabe == 9)
					aufgabe8();
				else if (aufgabe == 10)
					aufgabe9();
				else if (aufgabe == 11)
					aufgabe10();
			} while (!(aufgabe >= 1 && aufgabe <= 11));
		} while (abfrage() == 'y');
	}

	public static void aufgabe1() {

		System.out.print("geben sie die Note ein: ");
		byte note = tastatur.nextByte();
		if (note == 1)
			System.out.println("Sehr gut");

		else if (note == 2)
			System.out.println("Gut");

		else if (note == 3)
			System.out.println("Befriedigend");

		else if (note == 4)
			System.out.println("Ausreichend");

		else if (note == 5)
			System.out.println("Mangelhaft");

		else if (note == 6)
			System.out.println("Ungenügend");
		
		else
			System.out.println("Ungültige Note");
	}

	public static void aufgabe2() {

		System.out.print("bitte geben sie den Nettowert ein:");
		double netto = tastatur.nextDouble();

		System.out.println(
				"entscheidet sich über die Eingabe von „j“ für den ermäßigten Steuersatz und mit Eingabe von „n“ für den vollen Steuersatz: ");
		char st = tastatur.next().charAt(0);
		if (st == 'j')
			System.out.printf("%.2f %s\n", netto * 1.16, "€");

		else if (st == 'n')
			System.out.printf("%.2f %s\n", netto * 1.05, "€");

		else
			System.out.print("Falsche eingabe");

	}

	public static void aufgabe3a() {

		System.out.print("bitte geben sie die Ziffer des Monats ein: ");
		byte zahl = tastatur.nextByte();

		if (!(zahl >= 1 && zahl <= 12))
			System.out.println(zahl + "liegt nicht zwischen 1 und 12");
		else if (zahl == 1)
			System.out.println("Januar");
		else if (zahl == 2)
			System.out.println("Februar");
		else if (zahl == 3)
			System.out.println("März");
		else if (zahl == 4)
			System.out.println("April");
		else if (zahl == 5)
			System.out.println("Mai");
		else if (zahl == 6)
			System.out.println("Juni");
		else if (zahl == 7)
			System.out.println("Juli");
		else if (zahl == 8)
			System.out.println("August");
		else if (zahl == 9)
			System.out.println("September");
		else if (zahl == 10)
			System.out.println("Oktober");
		else if (zahl == 11)
			System.out.println("November");
		else
			System.out.println("Dezember");

	}

	public static void aufgabe3b() {

		System.out.print("bitte geben sie die Ziffer des Monats ein: ");
		byte zahl = tastatur.nextByte();

		switch (zahl) {
		case 1:
			System.out.println("Januar");
			break;
		case 2:
			System.out.println("Februar");
			break;
		case 3:
			System.out.println("März");
			break;
		case 4:
			System.out.println("April");
			break;
		case 5:
			System.out.println("Mai");
			break;
		case 6:
			System.out.println("Juni");
			break;
		case 7:
			System.out.println("Juli");
			break;
		case 8:
			System.out.println("August");
			break;
		case 9:
			System.out.println("September");
			break;
		case 10:
			System.out.println("Okotober");
			break;
		case 11:
			System.out.println("November");
			break;
		case 12:
			System.out.println("Dezember");
			break;
		default:
			System.out.println(zahl + "liegt nicht zwischen 1 und 12");
			break;
		}
	}

	public static void aufgabe4() {

		System.out.print("Wieviele Mäuse werden gekauft: ");
		byte artikel = tastatur.nextByte();
		
		System.out.print("Preis der Mäuse: ");
		double mbetrag = tastatur.nextDouble();

		if (artikel >= 10)
			System.out.printf("%s %.2f €\n","Mäuse werden Freihaus geliefert!",artikel * mbetrag);
		else
			System.out.printf("%s %.2f €\n","Mäuse werden mit einer Lieferpauschale geliefert!",1.16*(artikel * mbetrag + 10));
	}

	public static void aufgabe5() {

		System.out.print("Artikel Preis in Nettowert: ");
		short bestellwert = tastatur.nextShort();

		if (bestellwert >= 0 && bestellwert <= 100)
			System.out.printf("%s %.2f €\n", "Rabatt von 10%:", 1.16 * (bestellwert - (bestellwert * 0.10)));
		else if (bestellwert <= 500)
			System.out.printf("%s %.2f €\n", "Rabatt von 15%", 1.16 * (bestellwert - (bestellwert * 0.15)));
		else
			System.out.printf("%s %.2f €\n", "Rabatt von 20%:", 1.16 * (bestellwert - (bestellwert * 0.20)));
	}

	public static void aufgabe6() {

			System.out.print("Größe in (m):");
			double größe = tastatur.nextDouble();

			System.out.print("Gewicht in (Kg):");
			double gewicht = tastatur.nextDouble();

			System.out.print("Mann oder Frau  M/F:");
			char gesch = tastatur.next().charAt(0);

			double bmi = gewicht / Math.pow(größe, 2);
			
			if (gesch == 'F' && bmi < 19 || gesch == 'M' && bmi < 20) {
				System.out.printf("%s %.1f %s\n", "Dein BMI beträgt:", bmi, "du hast Untergewicht");
			}
			else if (gesch == 'F' && bmi >= 19 && bmi <= 24 || gesch == 'M' && bmi >= 20 && bmi <= 25) {
				System.out.printf("%s %.1f %s\n", "Dein BMI beträgt:", bmi, "du hast Normalgewicht");
			}
			else if (gesch == 'F' && bmi > 24 || gesch == 'M' && bmi > 25) {
				System.out.printf("%s %.1f %s\n", "Dein BMI beträgt:", bmi, "du hast Übergewicht");
			}
		}

	public static void aufgabe7() {

		System.out.print("bitte geben sie eine Römische Zahl ein (I,V,X,L,C,D,M):");
		char rzahl = tastatur.next().charAt(0);

		if (rzahl == 'I')
			System.out.println(1);
		else if (rzahl == 'V')
			System.out.println(5);
		else if (rzahl == 'X')
			System.out.println(10);
		else if (rzahl == 'L')
			System.out.println(50);
		else if (rzahl == 'C')
			System.out.println(100);
		else if (rzahl == 'D')
			System.out.println(500);
		else if (rzahl == 'M')
			System.out.println(1000);
	}

	public static void aufgabe8() {

		char neins = 0, nzwei = 0, ndrei = 0;

		System.out.print("zufällige Zahl oder Buchstabe:");
		char eins = tastatur.next().charAt(0);
		System.out.print("zufällige Zahl oder Buchstabe:");
		char zwei = tastatur.next().charAt(0);
		System.out.print("zufällige Zahl oder Buchstabe:");
		char drei = tastatur.next().charAt(0);

		if (eins <= zwei && eins <= drei)
			neins = eins;
		else if (zwei <= eins && zwei <= drei)
			neins = zwei;
		else if (drei <= zwei && drei <= eins)
			neins = drei;

		if (eins != neins && eins < zwei | eins < drei)
			nzwei = eins;
		else if (zwei != neins && zwei < eins | zwei < drei)
			nzwei = zwei;
		else if (drei != neins && drei < zwei | drei < eins)
			nzwei = drei;

		if (eins != nzwei && eins > zwei && eins > drei)
			ndrei = eins;
		else if (zwei != nzwei && zwei > eins && zwei > drei)
			ndrei = zwei;
		else if (drei != nzwei && drei > zwei && drei > eins)
			ndrei = drei;

		System.out.println(neins + " " + nzwei + " " + ndrei);

	}

	public static void aufgabe9() {

		System.out.print("Bitte das Jahr eingeben:");
		short jahr = tastatur.nextShort();

		if (jahr < 1582 && jahr > -45 && jahr % 4 == 0)
			System.out.println("Schaltjahr");
		else if (jahr > 1582 && jahr % 4 == 0 && jahr % 100 != 0 || jahr % 400 == 0)
			System.out.println("Schaltjahr");
		else
			System.out.println("kein Schaltjahr");
	}

	public static void aufgabe10() {

		System.out.println("Bitte geben Sie eine größere Römische zahl ein (I, V, X, L, C, D, M): ");
		String roman = tastatur.next();

		char[] chars = roman.toCharArray();
		char lastChar = ' ';
		int value = 0;

		for (int i = chars.length - 1  ; i >= 0; i--) {
			switch (chars[i]) {
			case 'I':
				if (lastChar == 'X' || lastChar == 'V')
					value -= 1;
				else
					value += 1;
				break;
			case 'V':
				value += 5;
				break;
			case 'X':
				if (lastChar == 'C' || lastChar == 'L')
					value -= 10;
				else
					value += 10;
				break;
			case 'L':
				value += 50;
				break;
			case 'C':
				if (lastChar == 'M' || lastChar == 'D')
					value -= 100;
				else
					value += 100;
				break;
			case 'D':
				value += 500;
				break;
			case 'M':
				value += 1000;
				break;
			}
			lastChar = chars[i];
		}
		System.out.println(value);
	}
}
