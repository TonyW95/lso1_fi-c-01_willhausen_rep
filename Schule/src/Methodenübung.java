import java.util.Scanner;

public class Methodenübung {
	
	public static double beschleunigung(double v, double dv) {
	 if(v + dv <= 130 && v + dv >= 0 ) {
		v += dv  ;
	}else 
		if(v + dv < 0) {
			v = 0;
		}
		else 
		{
			v = 130;
		}
		return v;		
	}

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
	double v = 0.0;
	double dv;
	
	System.out.printf("Startgeschwindigkeit des Fahrsimulators: %.0f %s \n", v ,"km/h");
	
	do
	{
	System.out.print("änderung der Geschwindigkeit: ");
	dv = tastatur.nextDouble();

	v = beschleunigung(v, dv);
	
	System.out.printf("aktuelle Geschwindigkeit des Fahrsimulators: %.0f %s \n ", v ,"km/h");
	}
	while (v > 0);
	}

}
