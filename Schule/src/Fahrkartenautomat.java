﻿import java.util.Scanner;

class Fahrkartenautomat {

	static Scanner tastatur = new Scanner(System.in);

	// fahrkartenbestellungErfassung
	public static double fahrkartenbestellungErfassung() {

//		1   Einzelfahrschein Berlin AB  2,90
//		2	Einzelfahrschein Berlin BC	3,30
//		3	Einzelfahrschein Berlin ABC	3,60
//		4	Kurzstrecke	1,90
//		5	Tageskarte Berlin AB	8,60
//		6	Tageskarte Berlin BC	9,00
//		7	Tageskarte Berlin ABC	9,60
//		8	Kleingruppen-Tageskarte Berlin AB	23,50
//		9	Kleingruppen-Tageskarte Berlin BC	24,30
//		10	Kleingruppen-Tageskarte Berlin ABC	24,90

		double preis[] = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
		String ticket[] = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
				"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };

		double fahrkarte1 = 2.90;
		double fahrkarte2 = 8.60;
		double fahrkarte3 = 23.50;
		double zuZahlenderBetrag = 0;
		byte anzahlFahrkarten;
		byte fahrkarte = 0;

//	Aufabe 6++		
		while (true) {
			System.out.println("\nWählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
			System.out.printf("Einzelfahrschein Regeltarif AB [%.2f EUR] (1)\n", fahrkarte1);
			System.out.printf("Tageskarte Regeltarif AB [%.2f EUR] (2)\n", fahrkarte2);
			System.out.printf("Kleingruppen-Tageskarte Regeltarif AB [%.2f EUR] (3)\n\n", fahrkarte3);
			
			if (zuZahlenderBetrag > 0) {
			System.out.println("Bezahlen (9) \n\n");
			}

			System.out.print("Ihre Wahl:");
			fahrkarte = tastatur.nextByte();
			if (fahrkarte >= 1 && fahrkarte <= 3) {
				System.out.print("Wieviele Fahrkarten möchten Sie ?: ");
				anzahlFahrkarten = tastatur.nextByte();
				switch (fahrkarte) {
				case 1:
					zuZahlenderBetrag += fahrkarte1 * anzahlFahrkarten;
					break;
				case 2:
					zuZahlenderBetrag += fahrkarte2 * anzahlFahrkarten;
					break;
				case 3:
					zuZahlenderBetrag += fahrkarte3 * anzahlFahrkarten;
					break;
				}
				continue;
			}
			if (fahrkarte == 9 && zuZahlenderBetrag > 0) {
				return zuZahlenderBetrag;
			} else
				System.out.println("Falsche Ticketnummer, bitte eingabe Wiederholen.");
		}
	}

//	Aufgabe 6
//	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
//	System.out.printf("Einzelfahrschein Regeltarif AB [%.2f EUR] (1)\n", fahrkarte1);
//	System.out.printf("Tageskarte Regeltarif AB [%.2f EUR] (2)\n", fahrkarte2);
//	System.out.printf("Kleingruppen-Tageskarte Regeltarif AB [%.2f EUR] (3)\n\n", fahrkarte3);
//	
//	
//	while(!(fahrkarte >= 1 && fahrkarte <= 3)) {
//		System.out.print("Ihre Wahl:");
//		fahrkarte = tastatur.nextByte();
//	if(fahrkarte == 1)
//		zuZahlenderBetrag = 2.90;
//	 else if (fahrkarte == 2)
//		 zuZahlenderBetrag = 8.60;
//	  else if (fahrkarte== 3) 
//		  zuZahlenderBetrag = 23.50;
//	  else
//		  System.out.println("falsche Eingabe");
//		}
// }

//		// Aufgabe 5
//		System.out.print("Zu zahlender Betrag (EURO): ");
//		zuZahlenderBetrag = tastatur.nextDouble();
//
//		if (zuZahlenderBetrag <= 0) {
//			zuZahlenderBetrag = 1;
//			System.out.println("Falsche eingabe, es wird einem Betrag von 1€ weiter berechnet\n");
//		}
//
//		System.out.print("Wieviele Fahrkarten möchten Sie (1-10) ?: ");
//		anzahlFahrkarten = tastatur.nextByte();
//
//		if (!(anzahlFahrkarten <= 10 && anzahlFahrkarten >= 1)) {
//			anzahlFahrkarten = 1;
//			System.out.println("Falsche eingabe, es wird mit einer Fahrkarte weiter berechnet\n");
//		}
//
//		return zuZahlenderBetrag * anzahlFahrkarten;
//	}
//
//		// Aufgabe 5++
//		System.out.print("Zu zahlender Betrag (EURO): ");
//		zuZahlenderBetrag = tastatur.nextDouble();
//
//		if (zuZahlenderBetrag <= 0) {
//			zuZahlenderBetrag = 1;
//			System.out.println("Falsche eingabe, es wird einem Betrag von 1€ weiter berechnet\n");
//		}
//		do {
//			System.out.print("Wieviele Fahrkarten möchten Sie (1-10) ?: ");
//			anzahlFahrkarten = tastatur.nextByte();
//		} while (!(anzahlFahrkarten <= 10 && anzahlFahrkarten >= 1));
//
//		return zuZahlenderBetrag * anzahlFahrkarten;
//	}

	// fahrkartenBezahlen
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		double eingeworfeneMünze;
		double ergebnis = 0.0;
		while (ergebnis < zuZahlenderBetrag) {
			System.out.printf("Noch zu Zahlender Betrag: %.2f %s\n", zuZahlenderBetrag - ergebnis, "Euro");
			System.out.print("Einwurf (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			ergebnis += eingeworfeneMünze;
		}
		return ergebnis;
	}

	// fahrkartenAusgeben
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// rueckgeldAusgabe

	public static void rueckgeldAusgabe(double fahrkartenBezahlen, double fahrkartenbestellungErfassung) {
		double rückgabebetrag;

		rückgabebetrag = fahrkartenBezahlen - fahrkartenbestellungErfassung + 0.001;
		if (rückgabebetrag > 0.001) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f %s\n", rückgabebetrag, "Euro");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

		}
		while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
		{
			muenzeAusgeben(2, "€");
			rückgabebetrag -= 2.0;
		}

		while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
		{
			muenzeAusgeben(1, "€");
			rückgabebetrag -= 1.0;
		}
		while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
		{
			muenzeAusgeben(50, "Cent");
			rückgabebetrag -= 0.5;
		}
		while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
		{
			muenzeAusgeben(20, "Cent");
			rückgabebetrag -= 0.2;
		}
		while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
		{
			muenzeAusgeben(10, "Cent");
			rückgabebetrag -= 0.1;
		}
		while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
		{
			muenzeAusgeben(5, "Cent");
			rückgabebetrag -= 0.05;
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n\n");
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

	public static void main(String[] args) {

		while (true) {
			System.out.println("Fahrkartenbestellvorgang:");
			for (int i = 0; i < 10; i++) {
				System.out.print("=");
				warte(250);
			}

			System.out.print("\n\n");

			double zuZahlenderBetrag = fahrkartenbestellungErfassung();

			double ergebnis = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();

			rueckgeldAusgabe(ergebnis, zuZahlenderBetrag);
		}
	}
}