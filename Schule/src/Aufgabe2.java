
public class Aufgabe2 {

	public static void main(String[] args) {
		System.out.printf("%-4s = %-17s = %3s\n", "0!", "", 1);
		System.out.printf("%-4s = %-17s = %3s\n", "1!", "1", 1);
		System.out.printf("%-4s = %-17s = %3s\n", "2!", "1 * 2", 1*2);
		System.out.printf("%-4s = %-17s = %3s\n", "3!", "1 * 2 * 3", 1*2*3);
		System.out.printf("%-4s = %-17s = %3s\n", "4!", "1 * 2 * 3 * 4", 1*2*3*4);
		System.out.printf("%-4s = %-17s = %3s\n", "5!", "1 * 2 * 3 * 4 * 5", 1*2*3*4*5);
	}

}